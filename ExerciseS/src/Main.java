import java.util.SortedSet;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {
        SortedSet<Person> personSet = new TreeSet<>();
        personSet.add(new Person("Mary", "O Neill"));
        personSet.add(new Person("Joe", "Murphy"));
        personSet.add(new Person("Bob", "Power"));
        personSet.add(new Person("James", "Hamm"));

        System.out.println("People ordered by last name:");
        for(Person person : personSet) {
            System.out.println(person.getLastName() + ", " + person.getFirstName());
        }

    }
}
