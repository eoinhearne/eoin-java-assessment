class Person {

    private String firstName;
    private String lastName;
    private String address;
    private String phoneNo;

    Person(String firstName, String lastName, String address, String phoneNo) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNo = phoneNo;
    }

    String getFirstName() {
        return this.firstName;
    }

    void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    String getLastName() {
        return this.lastName;
    }

    void setLastName(String lastName) {
        this.lastName = lastName;
    }

    String getFullName() {
        return getFirstName() + " " + getLastName();
    }

    String getAddress() {
        return this.address;
    }
    void setAddress(String address) {
        this.address = address;
    }

    String getPhoneNo() {
        return this.phoneNo;
    }

    void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    @Override
    public String toString() {
        return ("Name: " + getFullName() + "\n" +
                "Address: " + getAddress() + "\n" +
                "Phone no: " + getPhoneNo());
    }
}
