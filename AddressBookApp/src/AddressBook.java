import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import static java.util.Collections.*;

class AddressBook {

    ArrayList<Person> personList = new ArrayList<>();
    private Scanner input = new Scanner(System.in);

    AddressBook(Person... personArray) {
        personList.addAll(Arrays.asList(personArray));
    }


    void mainMenu() {

        String instructions = "\nType the number of menu item required and press enter.";
        String menu = "Main Menu\n" +
                "1. Add person to address book.\n" +
                "2. Remove person from address book.\n" +
                "3. Edit person in address book.\n" +
                "4. View person's information.\n" +
                "5. View all people in address book\n" +
                instructions + "\n";

        System.out.println(menu);
        int menuChoice = input.nextInt();
        input.nextLine();
        switch(menuChoice) {
            case 1:
                addPersonMenu();
                break;
            case 2:
                removePersonMenu();
                break;
            case 3:
                editPersonMenu();
                break;
            case 4:
                viewPersonMenu();
            case 5:
                listAllMenu();
                break;
            default:
                System.out.println(menuChoice + " is not an option. Please try again.");
                mainMenu();
        }
    }

    void addPersonMenu() {
        System.out.println("Add Person");
        System.out.println("Enter first name: ");
        String firstName = input.nextLine();
        System.out.println("Enter last name: ");
        String lastName = input.nextLine();
        System.out.println("Enter address: ");
        String address = (String)input.nextLine();
        System.out.println("Enter phone number: ");
        String phoneNo = (String) input.nextLine();

        addPerson(new Person(firstName, lastName, address, phoneNo));
        System.out.println();
        mainMenu();
    }

    void removePersonMenu() {
        System.out.println("Remove Person");
        //check if person exists and add to personFound list
        if(!personList.isEmpty()) {
            ArrayList<Person> personFound = findMultiPerson();
            if (!personFound.isEmpty()) {
                for(Person person : personFound) {
                    System.out.println(person);
                    System.out.println();
                    System.out.println("Delete this person from address book? Y/N");
                    String answer = input.nextLine().toLowerCase();

                    while (!answer.equals("y") && !answer.equals("yes") && !answer.equals("n") && !answer.equals("no")) {
                        System.out.println("Enter Y or N only!");
                        answer = input.nextLine().toLowerCase();
                    }
                    if(answer.toLowerCase().equals("y") || answer.toLowerCase().equals("yes")) {
                        removePerson(person);
                        System.out.println();
                        mainMenu();
                    }
                }
            } else {
                System.out.println("No entries found.");
                mainMenu();
            }
            System.out.println("No more entries found.");
            System.out.println();
            mainMenu();
        } else {
            addressBookEmpty();
        }
    }

    void listAllMenu() {
        listAll();
        String menu = "1. Sort by first name.\n" +
                "2. Sort by last name.\n" +
                "3. Exit to main menu.";
        System.out.println(menu);
        int answer = input.nextInt();
        while(answer < 1 && answer > 3) {
            System.out.println(answer + " is not a valid option!");
            answer = input.nextInt();
        }
        switch(answer) {
            case 1:
                listByFirstName();
                mainMenu();
            case 2:
                listByLastName();
                mainMenu();
            case 3:
                mainMenu();
        }
        input.nextLine();
        mainMenu();
    }

    void editPersonMenu() {
        System.out.println("Edit Person");
        //check if person exists and add to personFound list
        if(!personList.isEmpty()) {
            ArrayList<Person> personFound = findMultiPerson();
            if (!personFound.isEmpty()) {
                for(Person person : personFound) {
                    System.out.println(person);
                    System.out.println();
                    System.out.println("1. Edit first name.\n" +
                            "2. Edit last name.\n" +
                            "3. Edit address.\n" +
                            "4. Edit phone number.\n" +
                            "5. Next entry.");
                    int answer = input.nextInt();
                    input.nextLine();

                    while (answer < 1 && answer > 5) {
                        System.out.println(answer + " is not a valid option!");
                        answer = input.nextInt();
                    }
                    switch(answer) {
                        case 1:
                            System.out.println("Enter first name: ");
                            String firstName = input.nextLine();
                            updateFirstName(person, firstName);
                            mainMenu();
                            break;
                        case 2:
                            System.out.println("Enter last name: ");
                            String lastName = input.nextLine();
                            updateLastName(person, lastName);
                            mainMenu();
                            break;
                        case 3:
                            System.out.println("Enter address: ");
                            String address = input.nextLine();
                            updateAddress(person, address);
                            mainMenu();
                            break;
                        case 4:
                            System.out.println("Enter phone number: ");
                            String phoneNo = input.nextLine();
                            updatePhoneNo(person, phoneNo);
                            mainMenu();
                            break;
                        case 5:
                            break;
                    }
                }
            } else {
                System.out.println("No entries found.");
                mainMenu();
            }
            System.out.println("No more entries found.");
            System.out.println();
            mainMenu();
        } else {
            addressBookEmpty();
        }
    }

    void viewPersonMenu() {
        System.out.println("View Person");
        //check if person exists and add to personFound list
        if(!personList.isEmpty()) {
            ArrayList<Person> personFound = findMultiPerson();
            if (!personFound.isEmpty()) {
                for(Person person : personFound) {
                    System.out.println(person);
                    System.out.println();
                    System.out.println("Press enter for next person.");
                    input.nextLine();
                }
            } else {
                System.out.println("No entries found.");
                mainMenu();
            }
            System.out.println("No more entries found.");
            System.out.println();
            mainMenu();
        } else {
            addressBookEmpty();
        }
    }

    ArrayList<Person> findMultiPerson() {
        System.out.println("Enter first name: ");
        String firstName = input.nextLine();
        System.out.println("Enter last name: ");
        String lastName = input.nextLine();
        ArrayList<Person> foundPerson = new ArrayList<>();
        if (!personList.isEmpty()) {
            for(Person person : personList) {
                if(person.getFirstName().compareToIgnoreCase(firstName) == 0) {
                    if (person.getLastName().compareToIgnoreCase(lastName) == 0) {
                        foundPerson.add(person);
                    }
                }
            }
        }

        return foundPerson;
    }

    void addPerson(Person person) {
        personList.add(person);
        System.out.println(person.getFullName() + ": successfully added to address book.");
    }

    void removePerson(Person person) {
        personList.remove(person);
        System.out.println(person.getFullName() + ": successfully removed from address book.");
    }

    void updateFirstName(Person person, String newFirstName) {
        String oldName = person.getFullName();
        person.setFirstName(newFirstName);
        System.out.println(person.getFullName() + ": first name successfully updated.");
        System.out.println(oldName + " -> " + person.getFullName());
    }

    void updateLastName(Person person, String newLastName) {
        String oldName = person.getFullName();
        person.setLastName(newLastName);
        System.out.println(person.getFullName() + ": last name successfully updated.");
        System.out.println(oldName + " -> " + person.getFullName());
    }

    void updateAddress(Person person, String newAddress) {
        person.setAddress(newAddress);
        System.out.println(person.getFullName() + ": address successfully updated.");
        System.out.println(newAddress);
    }

    void updatePhoneNo(Person person, String newPhoneNo) {
        person.setPhoneNo(newPhoneNo);
        System.out.println(person.getFullName() + ": phone number successfully updated.");
        System.out.println(newPhoneNo);
    }

    void listAll() {
        if(!personList.isEmpty()) {
            for(Person person : personList) {
                System.out.println(person.getFullName());
            }
            System.out.println();
        } else {
            addressBookEmpty();
        }
    }

    void listByFirstName() {
        sort(personList, new FirstNameComparator());
        listAllMenu();
    }

    void listByLastName() {
        sort(personList, new LastNameComparator());
        listAllMenu();
    }

    void addressBookEmpty() {
        System.out.println("Address book is empty.");
    }
}
