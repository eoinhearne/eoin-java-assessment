import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Person person1 = new Person("Joe", "Murphy", "14 Upper Hill, Hillside, Cork", "8978768448");
        Person person2 = new Person("Mary", "Murphy", "14 Upper Hill, Hillside, Cork", "65465456654");
        Person person4 = new Person("Mary", "Murphy", "12 Upper Hill, Hillside, Cork", "564654");
        Person person3 = new Person("Bob", "O Neill", "24 Road Side, Upper Road, Waterford", "654654848564");

        AddressBook addressBook = new AddressBook(person1, person2, person3, person4);
        addressBook.mainMenu();
    }
}
