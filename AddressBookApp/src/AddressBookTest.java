import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.PrintStream;

public class AddressBookTest extends AddressBook {

    private Person person;
    private AddressBook addressBook = new AddressBook();

    @Before
    public void setUp() throws Exception {
        person = new Person("Anne", "Mc Cleary", "34 Hillside View, Waterford", "1651654654");
    }

    @Test
    public void testAddPerson() {
        addressBook.addPerson(person);
        assertEquals(addressBook.personList.get(0), person);
    }

    @Test
    public void testRemovePerson() {
        addressBook.addPerson(person);
        addressBook.removePerson(person);
        assertTrue(addressBook.personList.isEmpty());
    }

    @Test
    public void testUpdateFirstName() {
        assertEquals(person.getFirstName(), "Anne");
        updateFirstName(person, "Mary");
        assertEquals(person.getFirstName(), "Mary");
    }

    @Test
    public void testUpdateLastName() {
        assertEquals(person.getLastName(), "Mc Cleary");
        updateLastName(person, "Berry");
        assertEquals(person.getLastName(), "Berry");
    }

    @Test
    public void testUpdateAddress() {
        assertEquals(person.getAddress(), "34 Hillside View, Waterford");
        updateAddress(person, "39 Hillside View, Dublin");
        assertEquals(person.getAddress(), "39 Hillside View, Dublin");
    }

    @Test
    public void testUpdatePhoneNo() {
        assertEquals(person.getPhoneNo(), "1651654654");
        updatePhoneNo(person, "5646545");
        assertEquals(person.getPhoneNo(), "5646545");
    }

}