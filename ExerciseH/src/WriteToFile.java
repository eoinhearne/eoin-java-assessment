import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriteToFile {

    private File file;
    private String message;

    WriteToFile(String message, String filePath) {
        this.file = new File(filePath);
        this.message = message;
    }

    void writeToFile() {
        try (FileWriter writer = new FileWriter(file)){
            writer.write(message);
            System.out.println("Message written to file " + file + ": " + message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
