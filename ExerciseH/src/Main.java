public class Main {

    public static void main(String[] args) {
        String message = "This will be written to the file.";
        String filePath = "readme.txt";
        WriteToFile file = new WriteToFile(message, filePath);
        file.writeToFile();
    }
}
