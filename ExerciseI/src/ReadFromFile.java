import java.io.*;
import java.nio.Buffer;

public class ReadFromFile {

    private String filePath;


    ReadFromFile(String filePath) {
        this.filePath = filePath;
    }

    void readFromFile() {
        try(FileReader reader = new FileReader(filePath);
            BufferedReader bufferedReader = new BufferedReader(reader)) {
            String message = "";
            String line = bufferedReader.readLine();
            while(line != null) {
                message += line;
                line = bufferedReader.readLine();
            }
            System.out.println("Message read from file " + filePath + ": " + message);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
