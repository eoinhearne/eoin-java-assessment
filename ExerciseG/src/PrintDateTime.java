import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

class PrintDateTime {

    Date currentDate = new Date();

    void addOneDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        calendar.add(Calendar.DATE, 1);
        currentDate = calendar.getTime();
    }

    void printDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
        System.out.println(dateFormat.format(currentDate));
    }
}
