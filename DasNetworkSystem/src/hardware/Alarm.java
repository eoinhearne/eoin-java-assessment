package hardware;

import java.util.Date;

public class Alarm {

    private String name;
    private String remedy;
    private int id;
    private Date date = new Date();
    private boolean active;

    public Alarm(String name, String remedy, int id) {
        this.name = name;
        this.remedy = remedy;
        this.id = id;
        this.active = true;
    }

    void activate() {
        this.active = true;
    }

    public void deactivate() {
        this.active = false;
    }

    boolean isActive() {
        return this.active;
    }

    public String getName() {
        return this.name;
    }

    String getDate() {
        return this.date.toString();
    }

    String getRemedy() {
        return this.remedy;
    }

    int getId() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ALARM: " + this.name +
                "\nDATE: " + getDate() +
                "\nREMEDY: " + this.remedy;
    }
}
