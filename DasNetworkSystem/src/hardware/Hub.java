package hardware;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;

public class Hub extends Equipment implements Comparable {

    private Hashtable<String, Node> nodes = new Hashtable<>();

    public Hub (String name, String id) {
        super(name, id);
    }

    public Boolean isNodeNameUnique(String nodeName) {
        if (!nodes.isEmpty()) {
            Collection<Node> nodeObjects = nodes.values();
            for (Node node : nodeObjects) {
                if (node.getName().compareToIgnoreCase(nodeName) == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public Boolean isNodeIdUnique(String nodeId) {
        if (!nodes.isEmpty()) {
            Collection<Node> nodeObjects = nodes.values();
            for (Node node : nodeObjects) {
                if (node.getId().compareToIgnoreCase(nodeId) == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public Node getNodeByName(String nodeName) {
        Collection<Node> nodeObjects = this.nodes.values();
        for(Node node : nodeObjects) {
            if (node.getName().equalsIgnoreCase(nodeName)) {
                return node;
            }
        }
        return null;
    }

    public Node getNodeIdName(String nodeName) {
        Collection<Node> nodeObjects = this.nodes.values();
        for(Node node : nodeObjects) {
            if (node.getId().equalsIgnoreCase(nodeName)) {
                return node;
            }
        }
        return null;
    }

    public int getTotalAlarmCount() {
        int count = 0;
        Collection<Node> nodeObjects = getAllNodes();
        if(!nodeObjects.isEmpty()) {
            for(Node node : nodeObjects) {
                count += node.getActiveAlarmCount();
            }
        }
        count += getActiveAlarmCount();
        return count;
    }

    public Collection<Node> getAllNodes() {
        Collection<Node> nodes = this.nodes.values();
        return nodes;
    }

    ArrayList<String> getAllNodeNames() {
        Collection<Node> nodeObjects = this.nodes.values();
        if (nodeObjects.isEmpty()) {
            return null;
        } else {
            ArrayList<String> nodeNames = new ArrayList<>();
            for (Node node : nodeObjects) {
                nodeNames.add(node.getName());
            }
            return nodeNames;
        }

    }

    public void addNode(String id, Node node) {
        nodes.put(id, node);
    }

    int getNodeCount() {
        return getAllNodes().size();
    }

    public void removeNode(Node node) {
        this.nodes.remove(node.getId());
    }



    @Override
    public String toString() {
        Collection<Node> nodes = this.nodes.values();
        String status =
                "\nEquipment type: Hub" +
                super.toString() +
                "\nNode count: " + getNodeCount();

        return status;
    }

    @Override
    public int compareTo(Object o) {
        Hub hub = (Hub) o;
        return this.getName().compareToIgnoreCase(hub.getName());
    }

    @Override
    public int hashCode() {
        int hashCodeResult = 0;
        int factor = 29;
        hashCodeResult += factor * getName().hashCode();
        hashCodeResult += factor * getId().hashCode();

        return hashCodeResult;
    }

    @Override
    public boolean equals(Object object) {
        if(!(object instanceof Hub)) {
            return false;
        }
        Hub hub = (Hub) object;

        return hub.getName().equals(this.getName()) &&
                hub.getId().equals(this.getId());
    }
}
