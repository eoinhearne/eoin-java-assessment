package hardware;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collection;

public class Network {

    private ArrayList<Carrier> carriers = new ArrayList<>();

    public ArrayList<Carrier> getAllCarriers() {
        return this.carriers;
    }

    public Boolean isCarrierUnique(String newCarrier) {
        if(!carriers.isEmpty()) {
            for(Carrier carrier : carriers) {
                if((carrier.getName().compareToIgnoreCase(newCarrier) == 0) ||
                        (carrier.getId().compareToIgnoreCase(newCarrier) == 0))
                {
                    return false;
                }
            }
        }
        return true;
    }

    public void addCarrier(Carrier carrier) {
        this.carriers.add(carrier);
    }

    public Carrier getCarrierByName(String carrierName) {
        if (!carriers.isEmpty()) {
            for(Carrier carrier : carriers) {
                if (carrier.getName().equalsIgnoreCase(carrierName)) {
                    return carrier;
                }
            }
        }
        return null;
    }

    public Carrier getCarrierById(String carrierId) {
        for(Carrier carrier : carriers) {
            if (carrier.getId().equalsIgnoreCase(carrierId)) {
                return carrier;
            }
        }
        return null;
    }

    public void removeCarrier(Carrier carrier) {
        this.carriers.remove(carrier);
    }

    public int getHubCount() {
        int count = 0;
        if(!carriers.isEmpty()) {
            for(Carrier carrier : carriers) {
                count += carrier.getHubCount();
            }
        }
        return count;
    }

    public int getNodeCount() {
        int count = 0;
        if(!carriers.isEmpty()) {
            for(Carrier carrier : carriers) {
                count += carrier.getNodeCount();
            }
        }
        return count;
    }

    public int getTotalAlarmCount() {
        int count = 0;
        if(!carriers.isEmpty()) {
            for(Carrier carrier : carriers) {
                count += carrier.getTotalAlarmCount();
            }
        }
        return count;
    }

    public String networkAsJson() {
        Gson gson = new Gson();
        String jsonString = gson.toJson(this);
        return jsonString;
    }


    @Override
    public String toString() {
        String status =
                "\nTotal carriers : " + carriers.size() +
                "\nTotal hubs: " + getHubCount() +
                "\nTotal nodes: " + getNodeCount() +
                "\nTotal active alarms: " + getTotalAlarmCount() +
                "\n";
        return status;
    }
}
