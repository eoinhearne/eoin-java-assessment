package hardware;

import java.util.ArrayList;

public class Node extends Equipment implements Comparable {

    public Node(String name, String id) {
        super(name, id);
    }

    @Override
    public String toString() {
        String status =
                "Equipment type: Node" +
                super.toString();
        return status;
    }

    @Override
    public int compareTo(Object o) {
        Node node = (Node) o;
        return getName().compareToIgnoreCase(node.getName());
    }
}
