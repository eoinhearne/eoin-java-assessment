package hardware;

import java.util.ArrayList;

public class Equipment {

    private String name;
    private String id;
    private ArrayList<Alarm> alarms = new ArrayList<>();

    public Equipment(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public ArrayList<Alarm> getActiveAlarms() {
        ArrayList<Alarm> activeAlarms = new ArrayList<>();
        if (!this.alarms.isEmpty()) {
            for(Alarm alarm : this.alarms) {
                if (alarm.isActive()) {
                    activeAlarms.add(alarm);
                }
            }
        }
        return activeAlarms;
    }

    public int getActiveAlarmCount() {
        return getActiveAlarms().size();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return this.id;
    }

    public void addAlarm(Alarm alarm) {
        alarms.add(alarm);
    }

    String getAlarmTypes() {
        ArrayList<Alarm> activeAlarms = getActiveAlarms();
        String[] alarmTypes = new String[4];
        if (!activeAlarms.isEmpty()) {
            for(Alarm alarm : activeAlarms) {
                int alarmIndex = alarm.getId();
                if (alarmTypes[alarmIndex] == null) {
                    alarmTypes[alarmIndex] = alarm.getName();
                }
            }
        }
        String formattedAlarms = "";
        for(String alarm : alarmTypes) {
            if(alarm != null) {
                formattedAlarms += "\n" + alarm;
            }
        }
        return formattedAlarms;
    }

    public String getAllRemedies() {
        Alarm[] alarmTypes = new Alarm[4];
        ArrayList<Alarm> activeAlarms = getActiveAlarms();
        String remedies = "Equipment: " + getName();
        if (!activeAlarms.isEmpty()) {
            for(Alarm alarm : activeAlarms) {
                if (alarmTypes[alarm.getId()] == null) {
                    alarmTypes[alarm.getId()] = alarm;
                }
            }
            for (Alarm alarm : alarmTypes) {
                if(alarm != null) {
                    remedies += "\nAlarm type: " + alarm.getName() +
                            "\nRemedy: " + alarm.getRemedy() + "\n";
                }
            }
        } else {
            remedies += "\nNo active alarms.";
        }
        return remedies;
    }

    public void clearAlarms() {
        ArrayList<Alarm> activeAlarms = getActiveAlarms();
        if (!activeAlarms.isEmpty()) {
            for (Alarm alarm : activeAlarms) {
                alarm.deactivate();
            }
        }
    }

    @Override
    public String toString() {
        String status = "\nName: " + getName()  +
                "\nID: " + getId() +
                "\nActive alarms : " + getActiveAlarmCount() +
                "\nAlarm types : " + getAlarmTypes();
        return status;
    }
}
