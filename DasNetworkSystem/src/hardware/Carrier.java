package hardware;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;

public class Carrier {

    private Hashtable<String, Hub> hubs = new Hashtable<>();
    private String name;
    private String id;

    public Carrier(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public Collection<Hub> getAllHubs() {
        Collection<Hub> hubs = this.hubs.values();
        return hubs;
    }

    public Hub getHubByName(String hubName) {
        Collection<Hub> hubObjects = this.hubs.values();
        for(Hub hub : hubObjects) {
            if (hub.getName().equalsIgnoreCase(hubName)) {
                return hub;
            }
        }
        return null;
    }

    public Hub getHubById(String hubId) {
        Collection<Hub> hubObjects = this.hubs.values();
        for(Hub hub : hubObjects) {
            if (hub.getId().equalsIgnoreCase(hubId)) {
                return hub;
            }
        }
        return null;
    }

    public int getHubCount() {
        int count = getAllHubs().size();
        return count;
    }

    public int getNodeCount() {
        Collection<Hub> hubs = this.hubs.values();
        int count = 0;
        for (Hub hub : hubs) {
            count += hub.getNodeCount();
        }

        return count;
    }

    public void addHub(String id, Hub newHub) {
        this.hubs.put(id, newHub);
    }

    public void removeHub(Hub hub) {
        this.hubs.remove(hub.getId());
    }

    public Boolean isHubNameUnique(String hubName) {
        if (!hubs.isEmpty()) {
            Collection<Hub> hubObjects = hubs.values();
            for (Hub hub : hubObjects) {
                if (hub.getName().compareToIgnoreCase(hubName) == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public Boolean isHubIdUnique(String id) {
        if (!hubs.isEmpty()) {
            Collection<Hub> hubObjects = hubs.values();
            for (Hub hub : hubObjects) {
                if (hub.getId().compareToIgnoreCase(id) == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public int getTotalAlarmCount() {
        int count = 0;
        Collection<Hub> hubObjects = getAllHubs();
        if (!hubObjects.isEmpty()) {
            for (Hub hub : hubObjects) {
                count += hub.getTotalAlarmCount();
            }
        }
        return count;
    }

    public String getFormattedNetwork() {
        Collection<Hub> hubObjects = getAllHubs();
        String network = "-" + getName() + "\n";
        if(!hubObjects.isEmpty()) {
            for(Hub hub: hubObjects) {
                network += "\t-" + hub.getName() + "\n";
                Collection<Node> nodes = hub.getAllNodes();
                if (!nodes.isEmpty()) {
                    for(Node node: nodes) {
                        network += "\t\t-" + node.getName() + "\n";
                    }
                }
            }
        }
        return network;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() { return this.id;}

    @Override
    public String toString() {
        Collection<Hub> hubs = getAllHubs();
        String status = "\nCarrier name: " + this.name +
                "\nCarrier id: " + this.id +
                "\nHub count: " + getHubCount() +
                "\nNode count: " + getNodeCount();

        return status;
    }
}
