package error;

public class ErrorMessage {

    public final String ALREADY_EXISTS = " already exists.";
    public final String IS_NOT_VALID_OPTION = " is not a valid option.";
    public final String NOT_EXIST = " does not exist.";

}
