package test;

import hardware.Alarm;
import hardware.Hub;
import hardware.Node;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class HubTest {

    Hub hub;

    @Before
    public void setUp() throws Exception {
        Alarm alarm1 = new Alarm("Unit Unavailable", "Equipment is in maintenance or is disconnected. Contact maintenance personnel.", 0);
        Alarm alarm2 = new Alarm("Optical Loss", "Equipment is faulty or is experiencing interference. Contact maintenance personnel.", 1);
        hub = new Hub("HUB-Central-Waterford", "VDF-H");
        hub.addAlarm(alarm1);
        hub.addAlarm(alarm2);
        Node node1 = new Node("NODE-Central-Waterford-1", "VDF-N");
        node1.addAlarm(alarm1);
        node1.addAlarm(alarm2);
        hub.addNode(node1.getId(), node1);
    }

    @Test
    public void getActiveAlarmCount() {
        int expectedResult = 2;
        System.out.println(hub.getActiveAlarmCount());

        Assert.assertEquals(expectedResult, hub.getActiveAlarmCount());
    }

    @Test
    public void isNodeNameUnique() {
        assertTrue(hub.isNodeNameUnique("NODE-Central-Dublin-1"));
    }

    @Test
    public void isNodeIdUnique() {
        assertTrue(hub.isNodeIdUnique("VDF-N1"));
    }

    @Test
    public void getNodeByName() {
        assertNotNull(hub.getNodeByName("NODE-Central-Waterford-1"));
        assertNull(hub.getNodeByName("NODE-Central-Dublin-1"));
    }

    @Test
    public void getNodeIdName() {
        assertNotNull(hub.getNodeIdName("VDF-N"));
        assertNull(hub.getNodeIdName("VDF-N1"));
    }

    @Test
    public void testGetTotalAlarmCount() {
        int expectedResult = 4;
        Assert.assertEquals(expectedResult, hub.getTotalAlarmCount());
    }
}