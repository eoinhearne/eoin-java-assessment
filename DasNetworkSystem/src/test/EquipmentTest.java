package test;

import hardware.Alarm;
import hardware.Equipment;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class EquipmentTest {


    Equipment equipment;

    @Before
    public void setUp() throws Exception {
        Alarm alarm1 = new Alarm("Unit Unavailable", "Equipment is in maintenance or is disconnected. Contact maintenance personnel.", 0);
        Alarm alarm2 = new Alarm("Optical Loss", "Equipment is faulty or is experiencing interference. Contact maintenance personnel.", 1);
        this.equipment = new Equipment("Node-Central-Waterford", "VDF-H");
        this.equipment.addAlarm(alarm1);
        this.equipment.addAlarm(alarm2);
    }

    @Test
    public void getActiveAlarmCount() {
        int expectedResult = 2;
        Assert.assertEquals(expectedResult, equipment.getActiveAlarmCount());
        expectedResult = 0;
        equipment.clearAlarms();
        Assert.assertEquals(expectedResult, equipment.getActiveAlarmCount());
    }

    @Test
    public void clearAlarms() {
        assertNotNull(equipment.getActiveAlarms());
        equipment.clearAlarms();
        assertTrue(equipment.getActiveAlarms().isEmpty());
    }
}