package test;

import hardware.*;
import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class CarrierTest {

    Carrier vodafone;

    @Before
    public void setUp() throws Exception {
        vodafone = new Carrier("vodafone", "VDF");
        Alarm alarm1 = new Alarm("Unit Unavailable", "Equipment is in maintenance or is disconnected. Contact maintenance personnel.", 0);
        Alarm alarm2 = new Alarm("Optical Loss", "Equipment is faulty or is experiencing interference. Contact maintenance personnel.", 1);
        Hub hub = new Hub("HUB-Central-Waterford", "VDF-H");
        hub.addAlarm(alarm1);
        hub.addAlarm(alarm2);
        vodafone.addHub(hub.getId(), hub);
        Node node1 = new Node("NODE-Central-Waterford-1", "VDF-N");
        node1.addAlarm(alarm1);
        node1.addAlarm(alarm2);
        hub.addNode(node1.getId(), node1);
    }

    @Test
    public void testGetHubByName() {
        assertNotNull(vodafone.getHubByName("HUB-Central-Waterford"));
        assertNull(vodafone.getHubByName("HUB-Central-Dublin"));
    }

    @Test
    public void testGetHubById() {
        assertNotNull(vodafone.getHubById("VDF-H"));
        assertNull(vodafone.getHubById("MTR"));
    }

    @Test
    public void testGetHubCount() {
        int expectedResult = 1;
        assertEquals(expectedResult, vodafone.getHubCount());
    }

    @Test
    public void testGetNodeCount() {
        int expectedResult = 1;
        assertEquals(expectedResult, vodafone.getNodeCount());
    }

    @Test
    public void testIsHubNameUnique() {
        assertTrue(vodafone.isHubNameUnique("HUB-Central-Dublin"));
    }

    @Test
    public void testIsHubIdUnique() {
        assertTrue(vodafone.isHubIdUnique("MTR"));
    }

    @Test
    public void testGetTotalAlarmCount() {
        int expectedResult = 4;
        assertEquals(expectedResult, vodafone.getTotalAlarmCount());
    }
}