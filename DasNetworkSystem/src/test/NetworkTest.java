package test;

import org.junit.Assert;
import hardware.*;
import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class NetworkTest {

    Network network;

    @Before
    public void setUp() throws Exception {
        network = new Network();
        Carrier vodafone = new Carrier("vodafone", "VDF");
        Alarm alarm1 = new Alarm("Unit Unavailable", "Equipment is in maintenance or is disconnected. Contact maintenance personnel.", 0);
        Alarm alarm2 = new Alarm("Optical Loss", "Equipment is faulty or is experiencing interference. Contact maintenance personnel.", 1);
        Hub hub = new Hub("HUB-Central-Waterford", "VDF-H");
        hub.addAlarm(alarm1);
        hub.addAlarm(alarm2);
        vodafone.addHub(hub.getId(), hub);
        Node node1 = new Node("NODE-Central-Waterford-1", "VDF-N");
        node1.addAlarm(alarm1);
        node1.addAlarm(alarm2);
        hub.addNode(node1.getId(), node1);
        network.addCarrier(vodafone);
    }

    @Test
    public void isCarrierUnique() {
        assertFalse(network.isCarrierUnique("vodafone"));
        assertTrue(network.isCarrierUnique("meteor"));
    }

    @Test
    public void getCarrierByName() {
        assertNotNull(network.getCarrierByName("vodafone"));
        assertNull(network.getCarrierByName("meteor"));
    }

    @Test
    public void getCarrierById() {
        assertNotNull(network.getCarrierById("vdf"));
        assertNull(network.getCarrierById("meteor"));
    }

    @Test
    public void testGetHubCount() {
        int expectedResult = 1;
        Assert.assertEquals(expectedResult, network.getHubCount());
    }

    @Test
    public void testGetNodeCount() {
        int expectedResult = 1;
        Assert.assertEquals(expectedResult, network.getNodeCount());
    }

    @Test
    public void testGetTotalAlarmCount() {
        int expectedResult = 4;
        Assert.assertEquals(expectedResult, network.getTotalAlarmCount());
    }
}