import com.google.gson.Gson;
import error.ErrorMessage;
import hardware.*;

import java.io.*;
import java.util.*;

public class Application {

    private ErrorMessage error = new ErrorMessage();
    private Scanner input = new Scanner(System.in);
    private Network networkIreland = new Network();
    private String instructions = "Enter commands or type ? for help menu...";
    private String[] alarmTypes = new String[]{"Unit Unavailable", "Optical Loss", "Dark fibre", "Power Outage" };
    private String[] remedies = new String[]{
            "Equipment is in maintenance or is disconnected. Contact maintenance personnel.",
            "Equipment is faulty or is experiencing interference. Contact maintenance personnel.",
            "This equipment is not currently being used.",
            "Contact local electrical company for updated estimated resolution time."
    };

    Application() {
        String newCarrierName = "vodafone";
        String carrierId = "VDF";
        Carrier vodafone = new Carrier(newCarrierName, carrierId);
        Alarm alarm1 = new Alarm(alarmTypes[0], remedies[0], 0);
        Hub hub1 = new Hub("HUB-Central-Waterford", "VDF-H");
        hub1.addAlarm(alarm1);
        vodafone.addHub(hub1.getId(), hub1);
        Node node1 = new Node("NODE-Central-Waterford-1", "VDF-N");
        node1.addAlarm(alarm1);
        hub1.addNode(node1.getId(), node1);
        networkIreland.addCarrier(vodafone);

        mainMenu();
    }

    void mainMenu() {
        System.out.println("Main Menu\n");
        System.out.println(instructions);
        String userChoice = input.nextLine();
        String lowerUserChoice = userChoice.toLowerCase();

        if(userChoice.contains("add carrier")) {
            String newCarrier = userChoice.replaceAll("(?i)add carrier ", "");
            addCarrierMenu(newCarrier);
        } else if (lowerUserChoice.contains("list carriers")) {
            listCarriersMenu();
        } else if (lowerUserChoice.contains("list network")) {
            listNetworkMenu();
        } else if (lowerUserChoice.contains("export network")) {
            String directory = userChoice.replaceAll("(?i)export network ", "");
            exportNetworkMenu(directory);
        } else if (lowerUserChoice.contains("import network")) {
            String filePath = userChoice.replaceAll("(?i)import network ", "");
            importNetworkMenu(filePath);
        } else if (lowerUserChoice.contains("status")) {
            System.out.println(networkIreland);
            mainMenu();
        } else if (lowerUserChoice.contains("select carrier")) {
            String carrierName = userChoice.replaceAll("(?i)select carrier ", "");
            selectCarrierMenu(carrierName);
        } else if (userChoice.equals("?")) {
            mainHelpMenu();
        } else {
            System.out.println(userChoice + error.IS_NOT_VALID_OPTION);
            System.out.println();
            mainMenu();
        }

    }

    void mainHelpMenu() {
        System.out.println("Main Help Menu\n");
        System.out.println("FUNCTION\t\t\tCOMMAND");
        System.out.println("List network\t\tlist network");
        System.out.println("Network status\t\tstatus");
        System.out.println("Add new carrier\t\tadd carrier <carrier name>");
        System.out.println("List all carriers\tlist carriers");
        System.out.println("Select carrier\t\tselect carrier <carrier name or id>");
        System.out.println("Import network\t\timport network <file path>");
        System.out.println("Export network\t\texport network <directory path>");
        System.out.println();
        mainMenu();
    }

    void addCarrierMenu(String carrierName) {
        if (networkIreland.isCarrierUnique(carrierName)) {
            System.out.println("Enter carrier id:");
            String carrierId = input.nextLine();
            Carrier newCarrier = new Carrier(carrierName, carrierId);
            if(networkIreland.isCarrierUnique(carrierName)) {
                networkIreland.addCarrier(newCarrier);
                System.out.println(carrierName + " added to carriers.");
            } else {
                System.out.println(carrierName + error.ALREADY_EXISTS);
            }
        } else {
            System.out.println(carrierName + error.ALREADY_EXISTS);
        }
        mainMenu();
    }

    void listCarriersMenu() {
        ArrayList<Carrier> carriers = networkIreland.getAllCarriers();
        if (carriers.isEmpty()) {
            System.out.println("Carrier list is empty.");
        } else {
            for(Carrier carrier : carriers) {
                System.out.println(carrier.getId() + "\t\t" + carrier.getName());
            }
        }
        System.out.println();
        mainMenu();
    }

    void listNetworkMenu() {
        ArrayList<Carrier> carriers = networkIreland.getAllCarriers();
        if (carriers.isEmpty()) {
            System.out.println("Network is empty.");
        } else {
            for(Carrier carrier: carriers) {
                System.out.println(carrier.getFormattedNetwork());
            }
        }
        System.out.println();
        mainMenu();
    }

    void exportNetworkMenu(String directory) {
        String networkJson = networkIreland.networkAsJson();
        String fileName = directory + "/networkIreland1.json";
        File jsonFile = new File(fileName);
        try (FileWriter writer = new FileWriter(jsonFile)) {
            writer.write(networkJson);
            System.out.println(fileName + " successfully created.");
        } catch (IOException ex) {
            System.out.println("Please check file path is correct.");
            System.out.println("File Name: " + fileName);
        }
        mainMenu();
    }

    void importNetworkMenu(String filePath) {

        Gson gson = new Gson();
        File jsonFile = new File(filePath);

        try (Reader reader = new FileReader(jsonFile)) {
            networkIreland = gson.fromJson(reader, Network.class);
            System.out.println("Network successfully imported.");
        } catch (IOException ex) {
            System.out.println("Please check file path is correct.");
            System.out.println("File path: " + filePath);
        }

        mainMenu();
    }

    void selectCarrierMenu(String carrierName) {
        ArrayList<Carrier> carriers = networkIreland.getAllCarriers();
        if(carriers.isEmpty()) {
            System.out.println("Carrier list is empty.");
            System.out.println();
            mainMenu();
        } else {
            if (!networkIreland.isCarrierUnique(carrierName)) {
                //get carrier from either name or id
                Carrier carrier = (networkIreland.getCarrierByName(carrierName) != null) ?
                        networkIreland.getCarrierByName(carrierName) :
                        networkIreland.getCarrierById(carrierName);
                carrierName = carrier.getName();
                System.out.println("Carrier Menu");
                System.out.print(carrierName + " > ");
                String menuChoice = input.nextLine();
                String lowerMenuChoice = menuChoice.toLowerCase();
                if (menuChoice.equals("?")) {
                    carrierHelpMenu(carrierName);
                } else if (menuChoice.equalsIgnoreCase("home")) {
                    mainMenu();
                } else if (lowerMenuChoice.contains("delete carrier")) {
                    networkIreland.removeCarrier(carrier);
                    System.out.println(carrierName + " deleted\n");
                    mainMenu();
                } else if (lowerMenuChoice.contains("list hubs")) {
                    listHubsMenu(carrierName);
                } else if (menuChoice.equals("home")) {
                    mainMenu();
                } else if (lowerMenuChoice.contains("rename carrier")) {
                    renameCarrierMenu(carrier);
                } else if (lowerMenuChoice.contains("add hub")) {
                    String hubName = menuChoice.replaceAll("(?i)add hub ", "");
                    addHubMenu(carrier, hubName);
                } else if (lowerMenuChoice.contains("select hub")) {
                    String hubName = menuChoice.replaceAll("(?i)select hub ", "");
                    if(!(carrier.isHubNameUnique(hubName) &&
                            carrier.isHubIdUnique(hubName))) {
                        //get hub by either id or name
                        Hub hub = (carrier.getHubByName(hubName) != null) ?
                                carrier.getHubByName(hubName) :
                                carrier.getHubById(hubName);
                        hubName = hub.getName();
                        selectHubMenu(carrierName, hubName);
                    } else {
                        System.out.println(hubName + error.NOT_EXIST);
                        selectCarrierMenu(carrierName);
                    }
                    selectHubMenu(carrierName, hubName);
                } else if (menuChoice.equals("status")) {
                    System.out.println(carrier);
                    System.out.println();
                    selectCarrierMenu(carrierName);
                } else {
                    System.out.println(menuChoice + error.IS_NOT_VALID_OPTION);
                    selectCarrierMenu(carrierName);
                }
            } else {
                System.out.println(carrierName + error.NOT_EXIST);
                mainMenu();
            }
        }
    }

    void addHubMenu(Carrier carrier, String hubName) {
        String carrierName = carrier.getName();
        if (carrier.isHubNameUnique(hubName)) {
            System.out.println("Enter unique hub ID:");
            String hubId = input.nextLine();
            if (carrier.isHubIdUnique(hubId)) {
                Hub hub = new Hub(hubName, hubId);
                carrier.addHub(hubId, hub);
                System.out.println(hubName + " added to carrier " + carrierName);
                selectCarrierMenu(carrierName);
            } else {
                System.out.println(hubId + error.ALREADY_EXISTS);
                selectCarrierMenu(carrierName);
            }
        } else {
            System.out.println(hubName + error.ALREADY_EXISTS);
            selectCarrierMenu(carrierName);
        }
    }

    void carrierHelpMenu(String carrierName) {
        System.out.println("Carrier Help Menu\n");
        System.out.println("FUNCTION\t\t\tCOMMAND");
        System.out.println("Delete carrier\t\tdelete carrier");
        System.out.println("Rename carrier\t\trename carrier");
        System.out.println("List hubs\t\t\tlist hubs");
        System.out.println("Select hub\t\t\tselect hub <hub name or id>");
        System.out.println("Add hub\t\t\t\tadd hub <hub name>");
        System.out.println("View status\t\t\tstatus");
        System.out.println("Main menu\t\t\thome");
        System.out.println();
        selectCarrierMenu(carrierName);
    }

    void selectHubMenu(String carrierName, String hubName) {
        Carrier carrier = networkIreland.getCarrierByName(carrierName);
        Hub hub = carrier.getHubByName(hubName);
        System.out.print(carrierName + " > " + hubName + " > ");
        String menuChoice = input.nextLine();
        String lowerMenuChoice = menuChoice.toLowerCase();
        if (menuChoice.equals("?")) {
            hubHelpMenu(carrierName, hubName);
        } else if (menuChoice.equals("home")) {
            mainMenu();
        } else if (lowerMenuChoice.contains("list nodes")) {
            listNodesMenu(carrierName, hubName);
        } else if (lowerMenuChoice.contains("rename hub")) {
            renameHubMenu(carrier, hub);
        } else if (lowerMenuChoice.contains("manage alarms")) {
            manageAlarmsMenu(hub);
        } else if (lowerMenuChoice.contains("add node")) {
            String nodeName = menuChoice.replaceAll("(?i)add node ", "");
            if (hub.isNodeNameUnique(nodeName)) {
                System.out.println("Enter node unique id:");
                String nodeId = input.nextLine();
                if (hub.isNodeIdUnique(nodeId)) {
                    Node node = new Node(nodeName, nodeId);
                    hub.addNode(nodeId, node);
                    System.out.println(nodeName + " successfully added to " + hubName);
                } else {
                    System.out.println(nodeName + error.ALREADY_EXISTS);
                }
                selectHubMenu(carrierName, hubName);
            } else {
                System.out.println(nodeName + error.ALREADY_EXISTS);
                selectHubMenu(carrierName, hubName);
            }
        } else if (lowerMenuChoice.contains("delete hub")) {
            carrier.removeHub(hub);
            System.out.println(hubName + " successfully deleted.");
            selectCarrierMenu(carrierName);
        } else if (lowerMenuChoice.contains("select node")) {
            String nodeName = menuChoice.replaceAll("(?i)select node ", "");
            if(!(hub.isNodeNameUnique(nodeName) && hub.isNodeIdUnique(nodeName))) {
                Node node = (hub.getNodeByName(nodeName) != null) ? hub.getNodeByName(nodeName) : hub.getNodeIdName(nodeName);
                nodeName = node.getName();
                selectNodeMenu(carrierName, hubName, nodeName);
            } else {
                System.out.println(nodeName + error.NOT_EXIST);
                System.out.println();
                selectHubMenu(carrierName, hubName);
            }

        } else if (menuChoice.equals("status")) {
            System.out.println(hub);
            System.out.println();
            selectHubMenu(carrierName, hubName);
        } else {
            System.out.println(menuChoice + error.IS_NOT_VALID_OPTION);
            selectHubMenu(carrierName, hubName);
        }
    }

    void hubHelpMenu(String carrierName, String hubName) {
        System.out.println("Hub Help Menu\n");
        System.out.println("FUNCTION\t\t\tCOMMAND");
        System.out.println("List nodes\t\tlist nodes");
        System.out.println("Select node\t\tselect node <node name or id>");
        System.out.println("View status\t\tstatus");
        System.out.println("Delete hub\t\tdelete hub");
        System.out.println("Rename hub\t\trename hub");
        System.out.println("Manage alarms\tmanage alarms");
        System.out.println("Add node\t\tadd node <node name>");
        System.out.println("Main menu\t\thome");
        System.out.println();
        selectHubMenu(carrierName, hubName);
    }

    void selectNodeMenu(String carrierName, String hubName, String nodeName) {
        Carrier carrier = networkIreland.getCarrierByName(carrierName);
        Hub hub = carrier.getHubByName(hubName);
        Node node = hub.getNodeByName(nodeName);
        System.out.print(carrierName + " > " + hubName + " > " + nodeName + " > ");
        String menuChoice = input.nextLine();
        String lowerMenuChoice = menuChoice.toLowerCase();
        if (menuChoice.equals("?")) {
            nodeHelpMenu(carrierName, hubName, nodeName);
        } else if (menuChoice.equals("home")) {
            mainMenu();
        } else if (menuChoice.equals("status")) {
            System.out.println(node);
            System.out.println();
            selectNodeMenu(carrierName, hubName, nodeName);
        } else if (menuChoice.equals("manage alarms")) {
            manageAlarmsMenu(node);
        } else if (lowerMenuChoice.contains("delete node")) {
            hub.removeNode(node);
            System.out.println(nodeName + " successfully deleted from " + hubName);
            selectHubMenu(carrierName, hubName);
        } else if (lowerMenuChoice.contains("rename node")) {
            renameNodeMenu(carrier, hub, node);
        } else {
            System.out.println(menuChoice + error.IS_NOT_VALID_OPTION);
            selectNodeMenu(carrierName, hubName, nodeName);
        }
    }

    void nodeHelpMenu(String carrierName, String hubName, String nodeName) {
        System.out.println("Node Help Menu\n");
        System.out.println("FUNCTION\t\t\tCOMMAND");
        System.out.println("View status\t\tstatus");
        System.out.println("Delete node\t\tdelete node");
        System.out.println("Rename node\t\trename node");
        System.out.println("Manage alarms\tmanage alarms");
        System.out.println("Main menu\t\thome");
        System.out.println();
        selectNodeMenu(carrierName, hubName, nodeName);
    }

    void listHubsMenu(String carrierName) {
        Carrier carrier = networkIreland.getCarrierByName(carrierName);
        //convert collection to array list for sorting
        ArrayList<Hub> hubs = new ArrayList<>(carrier.getAllHubs());
        if (hubs.isEmpty()) {
            System.out.println(carrierName + " does not have any hubs.");
        } else {
            Collections.sort(hubs);
            for(Hub hub : hubs) {
                System.out.println(hub.getId() + "\t\t\t" + hub.getName());
            }
        }
        System.out.println();
        selectCarrierMenu(carrierName);
    }

    void listNodesMenu(String carrierName, String hubName) {
        Carrier carrier = networkIreland.getCarrierByName(carrierName);
        Hub hub = carrier.getHubByName(hubName);
        //convert collection to array list for sorting
        ArrayList<Node> nodes = new ArrayList<>(hub.getAllNodes());
        if(nodes.isEmpty()) {
            System.out.println(hubName + " does not have any nodes.");
        } else {
            Collections.sort(nodes);
            for(Node node: nodes) {
                System.out.println(node.getId() + "\t\t\t" + node.getName());
            }
        }
        System.out.println();
        selectHubMenu(carrierName, hubName);
    }

    void renameCarrierMenu(Carrier carrier) {
        String carrierName = carrier.getName();
        System.out.println("Enter new name for " + carrierName);
        String newCarrierName = input.nextLine();
        if (networkIreland.isCarrierUnique(newCarrierName)) {
            carrier.setName(newCarrierName);
            System.out.println(carrierName + " changed to " + newCarrierName);
            selectCarrierMenu(newCarrierName);
        } else {
            System.out.println(newCarrierName + error.ALREADY_EXISTS);
            renameCarrierMenu(carrier);
        }
    }

    void renameHubMenu(Carrier carrier, Hub hub) {
        String hubName = hub.getName();
        System.out.println("Enter new name for " + hubName);
        String newHubName = input.nextLine();
        if (carrier.isHubNameUnique(newHubName)) {
            hub.setName(newHubName);
            System.out.println(hubName + " changed to " + newHubName);
            selectHubMenu(carrier.getName(), newHubName);
        } else {
            System.out.println(newHubName + error.ALREADY_EXISTS);
            renameHubMenu(carrier, hub);
        }
    }

    void renameNodeMenu(Carrier carrier, Hub hub, Node node) {
        String nodeName = node.getName();
        System.out.println("Enter new name for " + nodeName);
        String newNodeName = input.nextLine();
        if (hub.isNodeNameUnique(newNodeName)) {
            node.setName(newNodeName);
            System.out.println(nodeName + " changed to " + newNodeName);
            selectNodeMenu(carrier.getName(), hub.getName(), newNodeName);
        } else {
            System.out.println(newNodeName + error.ALREADY_EXISTS);
            renameNodeMenu(carrier, hub, node);
        }
    }

    void manageAlarmsMenu(Equipment equipment) {
        System.out.println();
        System.out.println("Manage Alarms for " + equipment.getName());
        System.out.print(">");
        String menuChoice = input.nextLine().toLowerCase();
        if (menuChoice.equals("?")) {
            manageAlarmsHelpMenu(equipment);
        } else if (menuChoice.equals("home")) {
            mainMenu();
        } else if (menuChoice.equals("create alarm")) {
            createAlarmMenu(equipment);
        } else if (menuChoice.equals("clear alarm")) {
            clearAlarmMenu(equipment);
        } else if (menuChoice.equals("clear all alarms")) {
            clearAllAlarmsMenu(equipment);
        } else if (menuChoice.equals("remedies")) {
            viewRemediesMenu(equipment);
        } else {
            System.out.println(menuChoice + error.IS_NOT_VALID_OPTION);
            manageAlarmsMenu(equipment);
        }

    }

    void manageAlarmsHelpMenu(Equipment equipment) {
        System.out.println("Alarms Help Menu\n");
        System.out.println("FUNCTION\t\t\tCOMMAND");
        System.out.println("Create alarm\t\tcreate alarm");
        System.out.println("Clear alarm\t\t\tclear alarm");
        System.out.println("Clear all alarm\t\tclear all alarms");
        System.out.println("View remedies\t\tremedies");
        System.out.println("Main menu\t\t\thome");
        System.out.println();
        manageAlarmsMenu(equipment);
    }

    void createAlarmMenu(Equipment equipment) throws InputMismatchException {
        System.out.println("Create Alarm");
        System.out.println("Select corresponding alarm number to add to " + equipment.getName());
        for (int i = 0; i < alarmTypes.length; i++) {
            System.out.println((i + 1) + ". " + alarmTypes[i]);
        }
        int menuChoice = input.nextInt();
        input.nextLine();
        if (menuChoice >= 1 && menuChoice <= 4) {
            int alarmIndex = menuChoice - 1;
            Alarm alarm = new Alarm(alarmTypes[alarmIndex], remedies[alarmIndex], alarmIndex);
            equipment.addAlarm(alarm);
            if (alarm.getName().equalsIgnoreCase("Unit Unavailable") && (equipment instanceof Hub)) {
                Collection<Node> nodes = ((Hub) equipment).getAllNodes();
                if (!nodes.isEmpty()) {
                    for (Node node : nodes) {
                        node.addAlarm(alarm);
                        System.out.println(alarm.getName() + " successfully added to " + node.getName());
                    }
                }
            }
            System.out.println(alarm.getName() + " successfully added to " + equipment.getName());
            manageAlarmsMenu(equipment);
        } else {
            System.out.println("Invalid menu choice.");
            createAlarmMenu(equipment);
        }
    }

    void clearAlarmMenu(Equipment equipment) {
        ArrayList<Alarm> activeAlarms = equipment.getActiveAlarms();

        if (!activeAlarms.isEmpty()) {
            for(Alarm alarm : activeAlarms) {
                System.out.println(alarm);
                System.out.println();
                System.out.println("Clear above alarm? Y/N");
                String answer = input.nextLine().toLowerCase();
                if (answer.equals("y") || answer.equals("yes")) {
                    alarm.deactivate();
                    System.out.println("Alarm cleared.");
                }
            }
        } else {
            System.out.println("No active alarms on " + equipment.getName());
            manageAlarmsMenu(equipment);
        }
        System.out.println("End of alarm list.");
        manageAlarmsMenu(equipment);
    }

    void clearAllAlarmsMenu(Equipment equipment) {
        int alarmCount = equipment.getActiveAlarmCount();
        if (alarmCount > 0) {
            equipment.clearAlarms();
            System.out.println(alarmCount + " alarm(s) cleared.");
        } else {
            System.out.println("No active alarms on " + equipment.getName());
        }
        manageAlarmsMenu(equipment);

    }

    void viewRemediesMenu(Equipment equipment) {
        String remedies = equipment.getAllRemedies();
        System.out.println(remedies);
        manageAlarmsMenu(equipment);
    }




}
