# EoinJavaAssessment

My solutions to the assessments for introduction to Java. Each small project is separated into different folders beginning with 'Exercise'.

Another folder called AddressBookApp contains files and classes to run a basic address book application through the terminal. 

The advanced Java assessment is found in the DasNetworkSystem folder. The project is broken up into different packages:

## Packages

### error

- #### ErrorMessage
    Contains simple constant error strings used throughout the application.

### hardware

- #### Alarm

    Contains the parameters and methods used to add and remove alarms from different equipment.

- #### Carrier

    Contains methods used to control hubs within each carrier.

- #### Equipment 

    Super class of Hub and Node. Contains common methods shared by both.

- #### Hub

    Contains methods to manage nodes within a hub.

- #### Network

    Contains methods to manage carriers within a full network.

- #### Node

    Contains parameters used by nodes.

### test

Contains classes that unit test certain methods found in each classs using JUnit 4.

#### Application

Contains all the user input and menu options that make up the application.

#### Main

Contains the main method to create and run the application.



Compile and run the Main.java file for each project. 