import java.io.File;

class FileDirectory {

    //prints all files in directory including sub directories
    void printAllFiles(String filePath) {
        File directory = new File(filePath);
        File[] directoryContents = directory.listFiles();

        if (directoryContents != null) {
            for (File file : directoryContents) {
                //check amount of / in path to create tabAmount
                int tabAmount = 0;
                for(int i = 0; i < file.getPath().length(); i++) {
                    if(file.getPath().charAt(i) == '/') {
                        tabAmount++;
                    }
                }
                //create tab string
                String tabs = "";
                for(int i = 1; i < tabAmount; i++) {
                    tabs += "\t";
                }

                System.out.println(tabs + "-" + file.getName());
                //prints out subdirectory contents recursively
                if (file.isDirectory()) {
                    this.printAllFiles(file.getPath());
                }
            }
        }

    }

}
